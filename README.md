readiness - проверка готовности пода принять трафик. При провале проверки под выводится из списка эндпоинтов сервиса.
liveness - проверка жизнеспособности пода. При провале проверки под перезапускается.
Обе пробы активны на всём протяжении жизни пода, различия между ними только в том что произойдет при провале проверки.
Есть еще стартап проба, предназначенная для проверки запустился ли сервис в поде. Readiness и liveness проверки будут выполняться только после того как startup проба будет успешной. При провале startup пробы под перезапускается.

```bash
root@bastion:~# k get all -l app=nginx
NAME                        READY   STATUS    RESTARTS   AGE
pod/nginx-656fbd856-2q9cw   1/1     Running   0          8m30s
pod/nginx-656fbd856-98zp7   1/1     Running   0          8m30s
pod/nginx-656fbd856-d57j5   1/1     Running   0          8m30s
pod/nginx-656fbd856-fll9f   1/1     Running   0          8m30s
pod/nginx-656fbd856-rdq7f   1/1     Running   0          8m30s

NAME                         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/nginx-svc-cluster    ClusterIP   10.233.14.45    <none>        80/TCP         8m30s
service/nginx-svc-nodeport   NodePort    10.233.47.151   <none>        80:32390/TCP   8m30s

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx   5/5     5            5           8m30s

NAME                              DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-656fbd856   5         5         5       8m30s

root@bastion:~# curl master01:32390 -D - -o /dev/null -s
HTTP/1.1 200 OK
Server: nginx/1.21.3
Date: Fri, 22 Oct 2021 20:44:48 GMT
Content-Type: text/html
Transfer-Encoding: chunked
Connection: keep-alive
Expires: Fri, 22 Oct 2021 20:44:47 GMT
Cache-Control: no-cache

root@bastion:~# kubectl port-forward svc/nginx-svc-cluster 8888:80&
[1] 679055
Forwarding from [::1]:8888 -> 80

root@bastion:~# curl localhost:8888 -D - -o /dev/null -s
Handling connection for 8888
HTTP/1.1 200 OK
Server: nginx/1.21.3
Date: Fri, 22 Oct 2021 20:48:14 GMT
Content-Type: text/html
Transfer-Encoding: chunked
Connection: keep-alive
Expires: Fri, 22 Oct 2021 20:48:13 GMT
Cache-Control: no-cache
```
